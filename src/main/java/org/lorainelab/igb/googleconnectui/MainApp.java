package org.lorainelab.igb.googleconnectui;

import aQute.bnd.annotation.component.Component;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

@Component(immediate = true)
public class MainApp {

    private static Stage theStage;
    
    public static void startScreen() throws Exception {
       JFrame frame = new JFrame("Google Connect UI Development");
        final JFXPanel fxPanel = new JFXPanel();
        frame.add(fxPanel);
        frame.setSize(500, 400);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                VBox root;
                try {
                    URL resource = MainApp.class.getClassLoader().getResource("/fxml/FilePreview.fxml");
                    FXMLLoader loader = new FXMLLoader(resource);
                    loader.setController(new FXMLController());
                    loader.load();
                    root = loader.getRoot();
                    Scene  scene  =  new  Scene(root, Color.ALICEBLUE);
                    scene.getStylesheets().add(getClass().getResource("/styles/FilePreview.css").toExternalForm());
                    fxPanel.setScene(scene);
                } catch (IOException ex) {
                    Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */

    public static Stage getTheStage(){
        return theStage;
    }
    
    public static void startApp() {
        SwingUtilities.invokeLater(() -> {
            try {
                startScreen();
            } catch (Exception ex) {
                Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

}
