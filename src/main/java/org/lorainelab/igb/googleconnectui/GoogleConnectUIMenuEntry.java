/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.googleconnectui;

import aQute.bnd.annotation.component.Component;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuItem;

/**
 *
 * @author Deepti
 */
@Component(immediate = true)
public class GoogleConnectUIMenuEntry implements MenuBarEntryProvider{
//    private final String ICONPATH = "gDrive.png";
    private static final String GOOGLE_DRIVE_MENU_ITEM_LABEL = "Open File from Google Drive...";
    private static final int MENU_ITEM_WEIGHT = 8;

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem menuItem = new MenuItem(GOOGLE_DRIVE_MENU_ITEM_LABEL, (Void t) -> {
            try {
                MainApp.startApp();
            } catch (Exception ex) {
                Logger.getLogger(GoogleConnectUIMenuEntry.class.getName()).log(Level.SEVERE, null, ex);
            }
            return t;
        });

        menuItem.setWeight(MENU_ITEM_WEIGHT);

//        try (InputStream resourceAsStream = GoogleConnectUIMenuEntry.class.getClassLoader().getResourceAsStream(ICONPATH)) {
//            menuItem.setMenuIcon(new MenuIcon(resourceAsStream));
//        } catch (Exception ex) {
//        }

        return Optional.ofNullable(Arrays.asList(menuItem));
    }

    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.TOOLS;
    }
}
